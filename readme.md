## 1.
##### ETA 1 hour.

A binary search is not viable since we are limited to 2 fail cases.
A linear search would be the best method.

We could start the linear search along intervals of the square root of the total(intervals of 10). Once the Xbox breaks we have a range to continue a linear search on by intervals of 1, until we have the answer.

For this Method, the worst case scenario would be 19 tries.

The problem with this method is that the worst case scenario increases by 1 after every time the egg does not break along our first interval (10). So each iteration would have a worst case scenario of (numberOfTries + 10). To improve on this we can try to decrease the worst case scenario and make it more
consistent.

Using the equation n(n+1)/2 = 100 we can find an optimal interval to start from.
After each unbroken egg drop this interval will decrease by 1, keeping our worst case scenario at 14.

The equation n(n+1)/2 = 100 can be converted to the following javascript expression, rounded to up the the nearest whole number so that we have a floor to start from:

`Math.ceil((-.5) + Math.sqrt((.5^2)-(4*.5*-totalFloors)))`

Using this expression, we could write a linear(O(n)) algorithm to search for the exact floor an Xbox would break on, how many tries it would take to reach this floor, and the worst case scenario for any total amount of floors, with the worst cast scenario for 100 floors being 14.

## 2.
##### ETA 20 min.

webpage located at  [./question-2/index.html](./question-2/index.html)

    function integerToString(value, base){
        //make sure value is int
        value = parseInt(value);

        if(value === 0){
            return 0;
        }

        //declare output empty str
        var outputString = '';

        //get remainder of value/base and build string till value is zero
        while(value > 0){
            outputString = value%base + outputString;
            value = Math.floor(value/base);
        }

        return outputString;
    }

## 3. ETA 1 hr.
webpage located at  [./question-3/index.html](./question-3/index.html)

a. We could write tests to check against different intervals of the spiral, like the center of the spiral or each corner of the spiral.

## 4.

## 5.
##### ETA: 1 min.

This is an example of an XSS cookie hijacking attack. The Attacker can use
this to steal any sensitive information stored in a user's cookie, such as their
session token. If the user's session token is stored in their cookie, the
attacker can use this gain unauthorized access.

## 6.
##### ETA. 15 min.

At the application level of most threat models there are some general standards to follow. There are other defensive measures to take depending on
the technologies used in the application. A good mantra to follow for application level security is "never trust the client".

Backend queries should be parameterized and sanitized to prevent SQL injection. User input should be escaped. To defend against XSS attacks the front end should sanitize any values that are assigned to the DOM or executed. Sensitive information should be encrypted. Passwords should never be transmitted or stored in plaintext. Use tls / https / wss whenever possible.

There are tools available to provide additional information on overlooked defense practices during the implementation process or for newer and more obscure vulnerabilities. I use pen testing tools available on Kali Linux like sqlmap to expose some of these overlooked vulnerabilities. Performing regular code reviews is one of the best opportunities to spot overlooked weaknesses of an application.

## 7.
#### portfolio / code samples
- http://trickpatty.io
- https://gist.github.com/trickpattyFH20/c42ab7632cd2f714031ed89afdfe3ea9
- https://github.com/trickpattyFH20
